﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


/*
 * Director o "GameManager" antes lo unico que hacia era cambiar los textos al principio.
 * Ahora administra casi todo lo que pasa en el juego. Y use el patron Singleton para su facil acceso
 * Para el promedio de angulos use una lista la cual se va actualizando en cada Update(porque el ConfigPanel lo pide) pero se podria
 * cambiar facilmente para que me de valores por segundo, minuto, etc. El angulo muestra un valor con respecto a -x global.
 * */
public class Director : MonoBehaviour
{
    #region Singleton
    static Director instance;
    public static Director Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }
    #endregion Singleton

    int alienCount;
    float hits;
    float hitsPerSecond;
    List<float> angleList = new List<float>();

    float alienSpeed;
    float alienSpawnRate;

    public float AlienSpawnRate { get { return alienSpawnRate; } }
    public float AlienSpeed { get { return alienSpeed; } }
    public int AlienCount { get { return alienCount; } }
    public float HitsPerSecond { get { return hitsPerSecond; } }

    public float AngleListAverage
    {
        get
        {
            float avg = angleList.Average();
            angleList.Clear();
            if (alienCount != 0)return avg;
            return 0;
        }
    }

    void Start()
    {
        StartCoroutine(CounterHitsPerSecond());
    }

    public void AlienBorn()
    {
        alienCount++;
    }

    public void AlienDie()
    {
        alienCount--;
    }

    public void HitRegister()
    {
        hits++;
    }

    public void AddAngleToList(float angle)
    {
        angleList.Add(angle);
    }

    public void ChangeRate(float ammount)
    {
        alienSpawnRate = ammount;
    }
    public void ChangeSpeed(float ammount)
    {
        Alien.Speed = ammount;
    }

    IEnumerator CounterHitsPerSecond()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(1);
            hitsPerSecond = (float)hits / 1f;
            hits = 0;
        }
    }

}
