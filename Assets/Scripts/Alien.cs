﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;


/*
 * A este script le modifique muchas cosas. Partiendo por el sistema de cola(queue) para seguir un camino.
 * Esta forma lee la cola creada a partir del LevelInfo y crea una nueva exclusivamente para cada instancia del alien
 * Quite los getcomponents llamados en el update y animaciones que eran llamados constantementes para mejorar el rendimiento
 * Agregue las interacciones al colisionar con los diferentes enemigos.
 * Cambie el prefab del alien para que aparezca con su UI desde el inicio y cambie el texto de esta a un TMPro
 * para que sea mas legible. El sistema que use para que el terreno no tapara el texto es el de agregar otra camara que solo
 * renderizara el layer UI.
 * Como todos los aliens van a correr a la misma velocidad cree una variable de clase el cual controla este parametro.
 * Tambien arregle las animaciones para que funcione la animacion de caida.
 * 
 * */
public class Alien : MonoBehaviour
{

    [SerializeField] TMP_Text txtAlien;

    [SerializeField] static float speed;
    [SerializeField] [Range(3, 15)] float rotSpeed = 10;

    public static float Speed { set { speed = value; } }

    Animator animAlien;
    CharacterController ccAlien;
    Transform target;
    Queue<Transform> waypoints;

    int cantCocoonsTouching;
    [SerializeField] float angle;

    void Start()
    {        
        ccAlien = GetComponent<CharacterController>();
        animAlien = GetComponent<Animator>();   
    }

    public void SetAnimation()
    {
        if (!ccAlien.isGrounded)
        {
            animAlien.Play("Grounded");
            animAlien.SetFloat("Jump", 0);
        }
        else
        {
            animAlien.SetBool("OnGround", true);
            animAlien.SetFloat("Forward", 1f);
            animAlien.SetFloat("Jump", 1);
        }
    }


    void Update()
    {
        if (target != null) AlienMovement();

        angle = Vector3.Angle(this.transform.forward, -Vector3.right);
        Director.Instance.AddAngleToList(angle);

        SetAnimation();
    }

    void AlienMovement()
    {
        Vector3 delta = target.position - transform.position;
        delta.y = 0f;
        float deltaLen = delta.magnitude;
        float move = Mathf.Min(speed * Time.deltaTime, deltaLen);

        var direction = delta / deltaLen;
        transform.forward = Vector3.Slerp(transform.forward, direction, rotSpeed * Time.deltaTime);
        ccAlien.Move(move * transform.forward + Vector3.down * 3f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Waypoint"))
        {
            target = waypoints.Dequeue();
            return;
        }

        if (other.CompareTag("Cocoons"))
        {
            cantCocoonsTouching++;
            txtAlien.text = cantCocoonsTouching.ToString();
            Director.Instance.HitRegister();
            return;
        }

        if (other.CompareTag("FinishWaypoint"))
        {
            gameObject.SetActive(false);
            return;
        }
    }



    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Cocoons"))
        {
            cantCocoonsTouching--;
            txtAlien.text = cantCocoonsTouching.ToString();
        }
    }
    void OnEnable()
    {
        waypoints = new Queue<Transform>(LevelInfo.Instance.WaypointsQueue);
        target = waypoints.Dequeue();
        Director.Instance.AlienBorn();
        txtAlien.text = "0";
    }
    void OnDisable()
    {
        Director.Instance.AlienDie();
    }   
}
