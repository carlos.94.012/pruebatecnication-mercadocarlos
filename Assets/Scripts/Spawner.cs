﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*En el script del Spawner comence creando una corrutina para que maneje el spawn de los aliens
 * En el script del spawn original este se encargaba de Crear el alien, setearle su velocidad 
 * crear la UI que acompaña al Alien e iniciar las animaciones del alien.
 * De ahora en mas este script se encarga de lo que su nombre dicta, spawnear el Alien.
 * Todo lo relacionado al alien se migro al propio script "Alien"
 * Agregue un sistema de objectpooling para mejorar el rendimiento del juego
 * */
public class Spawner : MonoBehaviour
{
    Transform start;
    
    [SerializeField] GameObject prefabAlien;
    [SerializeField] List<GameObject> alienPoolList = new List<GameObject>();
    [SerializeField] Transform alienPool;
    int alienPoolSize = 3;
    float rateSpawn; 

    void Start()
    {
        CreateAlienPool(alienPoolSize);
        start = LevelInfo.Instance.StartWay;
        StartCoroutine(Spawn());
    }

    void CreateAlienPool(int ammount)
    {
        for (int i = 0; i < ammount; i++)
        {
            GameObject newAlien = Instantiate(prefabAlien.gameObject);
            newAlien.SetActive(false);
            alienPoolList.Add(newAlien);
            newAlien.transform.parent = alienPool;
        }
    }

    GameObject AlienRequest()
    {
        for (int i = 0; i < alienPoolList.Count; i++)
        {
            if (!alienPoolList[i].activeSelf)
            {
                alienPoolList[i].SetActive(true);
                return alienPoolList[i];
            }
        }
        CreateAlienPool(1);
        alienPoolList[alienPoolList.Count - 1].SetActive(true);
        return alienPoolList[alienPoolList.Count - 1];
    }
    void CreateAlien()
    {
        GameObject alien = AlienRequest();
        alien.transform.position = start.position;
    }

    IEnumerator Spawn()
    {
        while (true)
        {            
            yield return new WaitForSeconds(rateSpawn);
            rateSpawn = Director.Instance.AlienSpawnRate;
            CreateAlien();
        }
    }

}
