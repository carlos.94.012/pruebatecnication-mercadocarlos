﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/*
 * A este script lo modifique casi en su totalidad. Ahora este script maneja exclusivamente la UI 
 * Los valores de hitsporsec, cantidad de aliens y el promedio de angulos los trae desde el game manager.
 * */
public class ConfigPanel : MonoBehaviour
{
    [SerializeField] Slider sliderRate;
    [SerializeField] Slider sliderSpeed;
    [SerializeField] Text txtRate;
    [SerializeField] Text txtSpeed;
    [SerializeField] Text txtInfo;

    
    void Start () 
    {
        SliderRateChange();
        SliderSpeedChange();
    }

    void Update()
    {
        ShowInfo();
    }

    public void SliderRateChange()
    {
       Director.Instance.ChangeRate( Mathf.RoundToInt(sliderRate.value));
       txtRate.text = Mathf.RoundToInt(sliderRate.value).ToString();
    }

    public void SliderSpeedChange()
    {
        Director.Instance.ChangeSpeed(sliderSpeed.value);
        txtSpeed.text = sliderSpeed.value.ToString();
    }

    public void ShowInfo()
    {
        txtInfo.text = $"{Director.Instance.AlienCount} aliens alive"
            + $"\n{Director.Instance.HitsPerSecond} cocoons hits p/sec"
            + $"\n{Director.Instance.AngleListAverage:F2}° average angle"
            ;
    }
   

}
