﻿using System.Collections.Generic;
using UnityEngine;

// Cree este script porque queria guardar todos los presets del Nivel
// El sistema que use para el Nivel y el pathfinding de los Aliens fue mediante Waypoints
// El script toma todos los transform que estan dentro de "waypoints", lo guarda en una
// cola junto con el transform del final y posteriormente es utilizado
// por el alien para que siga el camino.
// Si en un futuro se quieren agregar mas niveles seria crear un prefab de este objeto
// y al agregarlo en otra escena modificar los waypoints para que sigan el camino como el
// terreno y listo. 


public class LevelInfo : MonoBehaviour
{
    #region Singleton
    static LevelInfo instance;
    public static LevelInfo Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }
    #endregion Singleton

    [SerializeField] Transform startWay;
    [SerializeField] Transform finishWay;
    [SerializeField] Transform waypoints;
    Queue<Transform> waypointsQueue = new Queue<Transform>();

    public Transform StartWay { get { return startWay; } }
    public Queue<Transform> WaypointsQueue { get { return waypointsQueue; } }

    void Start()
    {
        LoadQueue();
    }

    void LoadQueue()
    {

        for (int i = 0; i < waypoints.childCount; i++)
        {
            waypointsQueue.Enqueue(waypoints.GetChild(i));
        }

        waypointsQueue.Enqueue(finishWay);
    }
}
